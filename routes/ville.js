var express = require('express');
var router = express.Router();
var fetch = require("node-fetch");
const API_KEY_GEO = process.env.API_KEY_GEO || 'Your geocode.xyz api key';
const API_KEY_MAP = process.env.API_KEY_MAP || 'Your google map api key';
const url = "https://geocode.xyz/";

const getData = async (url, ville) => {
  try {
    var param = ville.split(' ').join('+').concat('?json=1&auth=', API_KEY_GEO);
    var url = url.concat(param)
    const response = await fetch(url);
    const json = await response.json();
    console.log(json);
    return json;
  } catch (error) {
    console.log(error);
  }
};

/* POST ville. */
router.post('/', async (req, res, next) => {
  var data = await getData (url, req.body.nom_ville);
  if (data.matches === null){
    res.render('ville', { 
      nom_ville: req.body.nom_ville.toUpperCase(),
      description: 'Unknown city',
    });
  } else {
      res.render('ville', { 
        nom_ville: req.body.nom_ville.toUpperCase(),
        description: req.body.description,
        longt: data.longt,
        latt: data.latt,
        apikey: API_KEY_MAP
      });
  }
});

module.exports = router;