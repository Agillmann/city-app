# City App

A Web Application made in Node.js and based on Express, for learning purposes.

Functionality: for any city specified by the user, it displays where it is on a map.

## Prerequisites

In order to run City App, you need [Node.js](https://nodejs.org/en/download/) installed on your system.

Also, you must provide values for the following environment variables:

- `PORT`: the port on which the web server should listen (e.g. `3000`)
- `API_KEY_GEO`: Your geocode.xyz API Key
- `API_KEY_MAP`: Your Google Map API Key

How to specify the API Keys? Go to the website of those services and ask for one! :-)

## Install and usage

These instructions can be followed to run City App from a Bash terminal:

```bash
$ git clone https://gitlab.com/Agillmann/city-app.git
$ cd city-app
$ npm install
$ PORT=3000 API_KEY_GEO=xxx API_KEY_MAP=yyy npm start
```

And then you can access the Web App by opening http://localhost:3000 on your web browser.
